#! /bin/bash

sleep 20

username=$(who -q)

if [[ $username == *martin* ]]; then
    cd ~/pyvenvs/python3/terminalwetter
    source bin/activate
    cd ~/gitrepos/open/terminalwetter
    python terminalwetter.py
    deactivate
fi
