![terminalwetter in action](https://gitlab.com/martinfischer/terminalwetter/raw/master/screenshot.png)

    sudo cp terminalwetter.service /etc/systemd/system
    sudo cp terminalwetter.timer /etc/systemd/system
    systemctl enable terminalwetter.timer
    systemctl start terminalwetter.timer
    systemctl list-timers
