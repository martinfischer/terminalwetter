"""
terminalwetter.py holt Wetterdaten und erstellt damit eine Textdatei, deren Inhalt dann im Terminal
angezeigt wird.
"""
import os
import re
import requests
from time import sleep
from datetime import datetime
from bs4 import BeautifulSoup
from textwrap import wrap


url_current = "http://wetter.orf.at/tirol/kirchdorf/#tag"
url_forecast = "http://wetter.orf.at/tirol/prognose"
dot_dir = os.path.expanduser("~/.terminalwetter")
outfile = os.path.join(dot_dir, "terminalwetter.txt")
notes_aktuell =  os.path.expanduser("~/Dropbox/notes/aktuell")
forecast_days = 5


icons = {
    "keine Daten": "⨂",
    "heiter": "☀",
    "wolkenlos": "☀",
    "wolkig": "⛅",
    "stark bewölkt": "☁",
    "leichter Regen": "☂",
    "leichter Regenschauer": "☂",
    "starker Regen": "☔",
    "starker Regenschauer": "☔",
    "leicht bewölkt mit leichtem Niederschlag": "⛅  ☂",
    "stark bewölkt mit leichtem Niederschlag": "☁  ☂",
    "stark bewölkt mit leichtem Niederschlag und Gewitter": "☁  ☂  ☇",
    "leicht bewölkt mit starkem Niederschlag": "⛅  ☔",
    "stark bewölkt mit starkem Niederschlag": "☁  ☔",
    "stark bewölkt mit starkem Niederschlag und Gewitter": "☁  ☔  ☇",
    "bedeckt mit starkem Niederschlag": "☁  ☔",
    "leicht bewölkt": "⛅",
    "Regenschauer": "⛈",
    "bedeckt mit schwerem Hagel und Gewitter": "☁  ☄☄  ☇",
    "Regen": "⛆",
    "bedeckt mit leichtem Niederschlag und Gewitter": "☁  ☂  ☇",
    "Gewitter": "☇",
    "leicht bewölkt mit starkem Niederschlag und Gewitter": "⛅  ☔  ☇",
    "bedeckt mit Schneeregen": "⛅  ❄  ⛆",
    "stark bewölkt mit Schneeregen": "☁  ❄  ⛆",
    "stark bewölkt mit starkem Schneefall": "☁  ❄  ❄",
    "leichter Schneeregen": "❄  ⛆",
    "leichter Schneefall": "❄",
    "Schneefall": "❄  ❄",
    "bedeckt mit starkem Schneefall": "☁  ❄  ❄",
    "stark bewölkt mit schwerem Hagel": "☁  ☄☄ ☄☄",
    "leichter Schneeschauer": "❄",
    "starker Schneeregen": "❄ ☔",
    "bedeckt": "☁",
    "bedeckt mit schwerem Hagel": "☁  ☄☄",
    "Schneeregenschauer": "❄ ⛈",
    "leichter Schneeregenschauer": "❄ ☂",
    "starker Schneefall": "❄  ❄",
    "leicht bewölkt mit Gewitter": "⛅  ☇",
    "bedeckt mit leichtem Niederschlag": "⛅  ☂",
    "neblig": "🌫",
}


class color:
    red = "\e[1;31;1;240m"
    green = "\e[1;32;1;240m"
    yellow = "\e[1;33;1;240m"
    darkblue = "\e[0;34;1;240m"
    blue = "\e[1;34;1;240m"
    magenta = "\e[1;35;1;240m"
    turquoise = "\e[1;36;1;240m"
    white = "\e[1;37;1;240m"
    gray = "\e[1;38;1;240m"
    orange = "\e[1;38;1;166m"
    end = "\e[0m"


def write_log(message):
    """
    Schreibt ein Logfile
    """
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M")
    message = "{}: terminalwetter: {}\n".format(timestamp, message)
    logfile = os.path.expanduser("~/.applog")
    with open(logfile, "a") as f:
        f.write(message)


def get_weather_current():
    """
    Holt die aktuellen Wetterdaten von url_current
    """
    r = requests.get(url_current)
    html = r.text
    soup = BeautifulSoup(html, "html.parser")

    data = {}

    data["wetter"] = soup.find_all(class_="weatherIcon")[0]["title"]
    if not data["wetter"]:
        data["wetter"] = "keine Daten"

    datatable = soup.find_all(class_="dataTable")[0].p.next_siblings
    datatablelist = list(datatable)
    data["temp"] = "{} °C".format(datatablelist[3].get_text().split()[1])
    data["wind"] = "{} {} km/h".format(
        datatablelist[7].get_text().split()[1],
        datatablelist[7].get_text().split()[2],
    )

    regex = re.compile("Luftfeuchtigkeit")
    data["luftfeuchtigkeit"] = "{} %".format(
        soup.find("span", string=regex).parent.get_text().split()[1]
    )

    data["lastupdate"] = soup.find_all(class_="lastUpdateMessage")[0].get_text().strip()

    return data


def get_weather_forecast(forecast_days):
    """
    Holt die Wettervorhersagedaten von url_forecast
    """
    r = requests.get(url_forecast)
    html = r.text
    soup = BeautifulSoup(html, "html.parser")

    data = {}

    for day in range(forecast_days):
        data[day] = {}
        data[day]["wochentag"] = soup.find_all("th", "dayCol")[day].get_text().split()[0]
        data[day]["wetter"] = soup.find_all(class_="weatherIcon")[day].get_text()
        data[day]["temp_morning"] = "{} °C".format(
            soup.find_all(class_="morning")[day].get_text().split()[0]
        )
        data[day]["temp_highest"] = "{} °C".format(
            soup.find_all(class_="highest")[day].get_text().split()[0]
        )

    fulltextWrapper = soup.find("div", {"class": "fulltextWrapper"})
    regex = re.compile("^Heute")
    all_h2 = fulltextWrapper.find_all("h2", string=regex)
    data["heute"] = []
    for h2 in all_h2:
        data["heute"].append(h2.get_text().strip())
        data["heute"].append(h2.find_next_sibling().get_text().strip())

    return data


def no_icon(icon_text):
    """
    Schreibt in meinen notes Ordner, wenn ein Icon fehlt
    """
    write_log("Kein Icon für: {}".format(icon_text))
    no_icon_file = os.path.join(notes_aktuell, "terminalwetter_icon: {}.md".format(icon_text))
    if not os.path.exists(no_icon_file):
        with open(no_icon_file, mode="w") as of:
            of.write("{}\n".format(icon_text))
        write_log("Kein Icon: md geschrieben: {}".format(no_icon_file))
    else:
        write_log("Kein Icon: md existiert schon: {}".format(no_icon_file))


def create_textfile(current_data, forecast_data):
    """
    Baut alles zusammen und schreibt die Textdatei
    """
    l = "|"
    endl = "\n"

    text = ""

    # Tag
    text += color.orange + "{:^39.39}".format("Aktuell (" + current_data["lastupdate"] + ")") + color.gray
    for i in range(2):
        text += l
        text += color.orange + "{:^39.39}".format(forecast_data[i]["wochentag"]) + color.gray
    text += endl

    # Leere Zeile
    text += "{:>40}{:>40}".format(l, l)
    text += endl

    # Icon
    wetter_icon_current = current_data["wetter"]
    icon_current = icons.get(wetter_icon_current, "KEIN ICON")
    if icon_current == "KEIN ICON":
        no_icon(wetter_icon_current)
    text += color.turquoise + "{:^39.39}".format(icon_current) + color.gray
    for i in range(2):
        text += l
        wetter_icon_forecast = forecast_data[i]["wetter"]
        icon_forecast = icons.get(wetter_icon_forecast, "KEIN ICON")
        if icon_forecast == "KEIN ICON":
            no_icon(wetter_icon_forecast)
        text += color.turquoise + "{:^39.39}".format(icon_forecast) + color.gray
    text += endl

    # Leere Zeile
    text += "{:>40}{:>40}".format(l, l)
    text += endl

    # Wetter
    wetter_current_wraped = wrap(current_data["wetter"], width=39)
    text += color.green + "{:^39.39}".format(wetter_current_wraped[0]) + color.gray
    for i in range(2):
        text += l
        wetter_forecast_wraped = wrap(forecast_data[i]["wetter"], width=39)
        text += color.green + "{:^39.39}".format(wetter_forecast_wraped[0]) + color.gray
    text += endl

    # WetterZeile2
    if len(wetter_current_wraped) > 1:
        text += color.green + "{:^39.39}".format(wetter_current_wraped[1]) + color.gray
        text += l
    else:
        text += "{:>40}".format(l)
    for i in range(2):
        wetter_forecast_wraped = wrap(forecast_data[i]["wetter"], width=39)
        if len(wetter_forecast_wraped) > 1:
            text += color.green + "{:^39.39}".format(wetter_forecast_wraped[1]) + color.gray
            if i < 1:
                text += l
        else:
            if i < 1:
                text += "{:>40}".format(l)
    text += endl

    # Aktuelle Temperatur & Morgentemperatur
    text += color.darkblue + "{:^39.39}".format("Temperatur: " + current_data["temp"]) + color.gray
    for i in range(2):
        text += l
        text += color.blue + "{:^39.39}".format("Morgentemperatur: " + forecast_data[i]["temp_morning"]) + color.gray
    text += endl

    # Wind/Luft & Höchsttemperatur
    text += color.yellow + "{:^39.39}".format("Wind & Luft: " + current_data["wind"] + " & " + current_data["luftfeuchtigkeit"]) + color.gray
    for i in range(2):
        text += l
        text += color.red + "{:^39.39}".format("Höchsttemperatur: " + forecast_data[i]["temp_highest"]) + color.gray
    text += endl

    # Mitte
    text += "{}\n".format("-" * 120)

    # Tag2
    for i in range(2, 5):
        text += color.orange + "{:^39.39}".format(forecast_data[i]["wochentag"]) + color.gray
        if i < 4:
            text += l
    text += endl

    # Leere Zeile
    text += "{:>40}{:>40}".format(l, l)
    text += endl

    # Icon2
    for i in range(2, 5):
        wetter_icon_forecast = forecast_data[i]["wetter"]
        icon_forecast = icons.get(wetter_icon_forecast, "KEIN ICON")
        if icon_forecast == "KEIN ICON":
            no_icon(wetter_icon_forecast)
        text += color.turquoise + "{:^39.39}".format(icon_forecast) + color.gray
        if i < 4:
            text += l
    text += endl

    # Leere Zeile
    text += "{:>40}{:>40}".format(l, l)
    text += endl

    # Wetter2
    for i in range(2, 5):
        wetter_forecast_wraped = wrap(forecast_data[i]["wetter"], width=39)
        text += color.green + "{:^39.39}".format(wetter_forecast_wraped[0]) + color.gray
        if i < 4:
            text += l
    text += endl

    # Wetter2Zeile2
    for i in range(2, 5):
        wetter_forecast_wraped = wrap(forecast_data[i]["wetter"], width=39)
        if len(wetter_forecast_wraped) > 1:
            text += color.green + "{:^39.39}".format(wetter_forecast_wraped[1]) + color.gray
            if i < 4:
                text += l
        else:
            if i < 4:
                text += "{:>40}".format(l)
    text += endl

    # Morgentemperatur2
    for i in range(2, 5):
        text += color.blue + "{:^39.39}".format("Morgentemperatur: " + forecast_data[i]["temp_morning"]) + color.gray
        if i < 4:
            text += l
    text += endl

    # Höchsttemperatur2
    for i in range(2, 5):
        text += color.red + "{:^39.39}".format("Höchsttemperatur: " + forecast_data[i]["temp_highest"]) + color.gray
        if i < 4:
            text += l
    text += endl

    # Heute
    for i in range(len(forecast_data["heute"])):
        if i % 2 == 0:
            text += endl + color.orange + forecast_data["heute"][i] + color.gray + endl
        else:
            long_text_wrapped = wrap(forecast_data["heute"][i], width=120)
            for wrapped_line in long_text_wrapped:
                text += color.turquoise + wrapped_line + color.gray + endl

    # Farbende
    text += color.end

    with open(outfile, mode="w") as of:
        of.write(text)


if __name__ == "__main__":
    """
    In .zshrc folgendes eintragen:
        echo -e "$(cat ~/.terminalwetter/terminalwetter.txt)"
    Im crontab folgendes eintragen:
        0,30 6-20 * * * /home/martin/workspace/terminalwetter/terminalwetter.sh
    """
    write_log("Gestartet")
    if not os.path.exists(dot_dir):
        os.mkdir(dot_dir)
        write_log("Neues dot_dir erstellt")
    try:
        current_data = get_weather_current()
        sleep(1)
        forecast_data = get_weather_forecast(forecast_days)
        create_textfile(current_data, forecast_data)
        write_log("Erfolgreich gelaufen")
    except Exception as error:
        write_log("FEHLER - {}".format(error))
